<footer class="site-footer border-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Enlaces y Redes Sociales</h3>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="index2.php">Inicio</a></li>
                  <li><a href="about.php">Sobre Nosotros</a></li>
                  <li><a href="index.php">Catálogo</a></li>
                  <li><a href="contact.php">Ubicación</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><span class="icon-facebook"></span><a href="https://www.facebook.com/Varos2020">  Facebook</a></li>
                  <li><span class="icon-instagram"></span><a href="https://www.instagram.com/">  Instagram</a></li>
                  <li><span class="icon-whatsapp"></span><a href="https://www.whatsapp.com/?lang=es">  Whatsapp</a></li>
                </ul>
              </div>

            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-5 mb-5">
              <h3 class="footer-heading mb-4">Ven a conocernos</h3>
              <ul class="list-unstyled">
                <li class="address">Av Costera Miguel Alemán, Deportivo, 39690 Acapulco de Juárez, Gro.</li>
                <li class="phone"><a href="tel://7445863481">+52 744 586 3481</a></li>
                <li class="email">www.varos.com</li>
              </ul>
            </div>

          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>Copyright &copy;
            <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
            <script>document.write(new Date().getFullYear());</script>Página oficial VARO'S</p>
          </div>
          
        </div>
      </div>
    </footer>