<!DOCTYPE html>
<html lang="en">
  <head>
    <title>VARO'S &mdash; E-commerce VARO'S</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v15.0" nonce="7bK5BXTX"></script>
  
  <div class="site-wrap">
    <?php include("./layouts/header.php"); ?> 

    <div class="site-section border-bottom" data-aos="fade">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-6">
            <div class="block-16">
              <figure>
                <img src="images/empezamos.jpg" alt="Imagen inicios varos" class="img-fluid rounded">
              </figure>
            </div>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-5">
            
            
            <div class="site-section-heading pt-3 mb-4">
              <h2 class="text-black justify-content-center">Como empezó todo...</h2>
            </div>
            <p>Somos una tienda con lista de extensos proveedores escogidos especialmente por su cálidad en ropa, comodidad y estilo. De esta forma, VARO's se ha ido consolidando para ofrecerte la mejor experiencia de compra.</p>
            <p>Gracias a ti y por confiar en nosotros, VARO's sigue creciendo para seguirte ofreciendo una mejor experiencia.</p>
            
          </div>
        </div>
      </div>
    </div>

    <div class="site-section border-bottom" data-aos="fade">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>Nuestras recomendaciones</h2>
          </div>
        </div>
        <div class="row">

          <div class="col-md-6 col-lg-4">
            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img src="images/user1.png" alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">Ángela Lopez</h3>
                </div>
                <div class="block-38-body">
                  <p>Mi mejor tienda para comprar en línea. Super recomendado y mi pedido llego antes de lo que esperaba. ¡Me encantaron las prendas! </p>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4">
            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img src="images/user2.png" alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">David Rojas</h3>
                </div>
                <div class="block-38-body">
                  <p> La meior calidad de ropa que he encontrado en linea. Muy rápido y los trabaiadores muy amables. Volveré en poco para sequir comprando mas ropa.</p>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4">
            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img src="images/user3.png" alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">Alberto Castro</h3>
                </div>
                <div class="block-38-body">
                  <p>Me dejó impresionado la relación calidad-precio de la tienda, tiene mucha variedad de ropa. ¡Sin duda se la recomendaré a mis amigos!</p>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    </br>
    <div class=" block-4  bg-light">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12  text-center pt-8">
          <h2>Queremos saber tus comentarios 👋🏼</h2>
          </br>
          <div class="fb-comments" data-href="http://132.248.75.198/~cuenta16/about.php" data-width="" data-numposts="3"></div>
         </div>
          </div>
        </div>
        </div>
        </div>





    <?php include("./layouts/footer.php"); ?> 
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>