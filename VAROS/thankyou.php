<?php
  session_start();
  include './php/conexion.php';
  if(!isset($_SESSION['carrito'])){
    header("Location: ./index.php");}
  $arreglo = $_SESSION['carrito'];
  $total = 0;
  for($i=0;$i<count($arreglo);$i++){
    $total = $total + ($arreglo[$i]['Precio']* $arreglo[$i]['Cantidad']);
  }
  $password="";
  if(isset($_POST['fcontrasena'])){
    if($_POST['fcontrasena']!=""){ //Si se ingresó contraseña para crear usuario, entonces creamos al usuario
      $password=$_POST['fcontrasena'];
    }
  }
  $id_usuario = $conexion -> insert_id;
  $conexion->query("insert into usuario (id,nombre,telefono,email,password)
  values(
    $id_usuario,
    '".$_POST['fnombre']." ".$_POST['fapellido']."',
    '".$_POST['fcel']."',
    '".$_POST['femail']."',
    '".sha1($password)."'
    )
  ") or die ($conexion->error);
  
  $fecha = date('Y-m-d h:m:s');
  //Inserción a la base de ventas
  $conexion -> query("insert into ventas(id_usuario,total,fecha) values (1,$total,'$fecha')") or die($conexion->error);
  $id_venta = $conexion->insert_id;
  for($i=0; $i<count($arreglo);$i++){ //Inserción a la tabla productos_venta

    $conexion -> query("insert into productos_venta(id_venta,id_producto,cantidad,precio,subtotal) values 
    ($id_venta,
    ".$arreglo[$i]['Id'].",
    ".$arreglo[$i]['Cantidad'].",
    ".$arreglo[$i]['Precio'].",
    ".$arreglo[$i]['Cantidad']*$arreglo[$i]['Precio']."
    )") or die($conexion->error);
    $conexion->query("update productos set inventario = inventario-".$arreglo[$i]['Cantidad']." where id=".$arreglo[$i]['Id'])or die($conexion->error); //Descontamos el elemento de cantidad a los de inventario 
    }
  $conexion->query("insert into envios(estado,ciudad,calle,num_calle,cp,id_venta) 
  values(
    '".$_POST['festado']."',
    '".$_POST['fciudad']."',
    '".$_POST['fcalle']."',
    '".$_POST['fnocalle']."',
    '".$_POST['fcp']."',
    $id_venta
  )
  
  ")or die($conexion->error);
  unset($_SESSION['carrito']); //Cerramos la sesión al insertar los elementos del carrito e información a la base
  
?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <title>VARO'S &mdash; E-commerce VARO'S</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">
   <?php include("./layouts/header.php"); ?> 

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <span class="icon-check_circle display-3 text-success"></span>
            <h2 class="display-3 text-black">Gracias por tu compra 🙌🏼</h2>
            <p class="lead mb-5">Tu compra fue completada correctamente.</p>
            <p><a href="index.php" class="btn btn-sm btn-primary">Regresar a la tienda</a></p>
          </div>
        </div>
      </div>
    </div>

    <?php include("./layouts/footer.php"); ?> 

  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/main.js"></script>

  </body>
</html>