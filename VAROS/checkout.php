<?php
  session_start();
  if(!isset($_SESSION['carrito'])){
    header('Location: ./index.php');
  }
  $arreglo=$_SESSION['carrito'];

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <title>VARO'S &mdash; E-commerce VARO'S</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">
    <?php include("./layouts/header.php"); ?> 
    
    <form action="./thankyou.php" method="post">

    <div class="site-section">
      <div class="container">
        
        <div class="row">
      
          <div class="col-md-6 mb-5 mb-md-0">
            <h2 class="h3 mb-3 text-black">Detalles de la compra</h2>
            <div class="p-3 p-lg-5 border">

              <div class="form-group row">
                <div class="col-md-6">
                  <label for="fnombre" class="text-black">Nombre <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="fnombre" name="fnombre">
                </div>
                <div class="col-md-6">
                  <label for="fapellido" class="text-black">Apellido <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="fapellido" name="fapellido">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <label for="festado" class="text-black">Estado </label>
                  <input type="text" class="form-control" id="festado" name="festado">
                </div>
              </div>

              <div class="form-group">
                <label for="fciudad" class="text-black"> <span class="text-danger">*</span></label>
                <select id="fciudad" class="form-control" name="fciudad">
                  <option value="1">Selecciona una ciudad</option>    
                  <option value="2">Acapulco</option>    
                  <option value="3">Zihuatanejo</option>    
   
                </select>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <label for="fcalle" class="text-black">Calle <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="fcalle" name="fcalle" placeholder="Nombre de la calle">
                </div>
              </div>


              <div class="form-group row">
              <div class="col-md-6">
                  <label for="fnocalle" class="text-black">No. Calle <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="fnocalle" name="fnocalle">
                </div>
                <div class="col-md-6">
                  <label for="fcp" class="text-black">Código postal <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="fcp" name="fcp">
                </div>
              </div>

              <div class="form-group row mb-5">
                <div class="col-md-6">
                  <label for="femail" class="text-black">E-mail <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="femail" name="femail">
                </div>
                <div class="col-md-6">
                  <label for="fcel" class="text-black">Celular <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="fcel" name="fcel" placeholder="Numero de Celular">
                </div>
              </div>

              <div class="form-group">
                <label for="c_create_account" class="text-black" data-toggle="collapse" href="#create_an_account" role="button" aria-expanded="false" aria-controls="create_an_account"><input type="checkbox" value="1" id="c_create_account"> Envia tu perfil</label>
                <div class="collapse" id="create_an_account">
                  <div class="py-2">
                    <p class="mb-3">Envíanos tu información para confirmar tu identidad al momento de contactarte para tu entrega</p>
                    <div class="form-group">
                      <label for="fcontrasena" class="text-black">Escribe una contraseña</label>
                      <input type="password" class="form-control" id="fcontrasena" name="fcontrasena" placeholder="">
                    </div>
                  </div>
                </div>
              </div>




            </div>
          </div>
          <div class="col-md-6">

   
            
            <div class="row mb-5">
              <div class="col-md-12">
                <h2 class="h3 mb-3 text-black">Tu orden</h2>
                <div class="p-3 p-lg-5 border">
                  <table class="table site-block-order-table mb-5">
                    <thead>
                      <th>Productos</th>
                      <th>Total</th>
                    </thead>
                    <tbody>
                      <?php
                      $total=0;
                      for($i=0;$i<count($arreglo);$i++){
                        $total = $total + ($arreglo[$i]['Precio']*$arreglo[$i]['Cantidad']);
                      ?>
                      <tr>
                        <td><?php echo $arreglo[$i]['Nombre'];?></td>
                        <td>$<?php echo number_format($arreglo[$i]['Precio'],2, '.','');?></td>
                      </tr>
                      <?php
                       }
                      ?>
                      <tr>
                        <td> Orden total</td>
                        <td>$<?php echo number_format($total,2, '.','');?></td>

                      </tr>

                    </tbody>
                  </table>

                  

                  <div class="form-group">
                    <button class="btn btn-primary btn-lg py-3 btn-block" type="submit">Completar compra</button>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- </form> -->
      </div>
    </div>
    </form>
    <?php include("./layouts/footer.php"); ?> 
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>