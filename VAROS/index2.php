<!DOCTYPE html>
<html lang="en">
  <head>
    <title>VARO'S &mdash; E-commerce VARO'S</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="css/aos.css">

   
    
  </head>
  <body>
  
  <div class="site-wrap">
  <header>
  <?php include("./layouts/header.php"); ?> 
    </header>

    <div class="site-blocks-cover" style="background-image: url(images/portada.png);" data-aos="fade">
      <div class="container">
        <div class="row align-items-start align-items-md-center justify-content-end">
          <div class="col-md-5 text-center text-md-left pt-5 pt-md-0">
            <h1 class="mb-2">Diseños exclusivos para tí</h1>
            <div class="intro-text text-center text-md-left">
              <p class="mb-4">Explora nuestro catálogo que hemos creado para tí</p>
              <p>
                <a href="index.php" class="btn btn-sm btn-primary red-theme-color">Ir a la tienda</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section site-section-sm site-blocks-1">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="">
            <div class="icon mr-4 align-self-start">
              <span class="icon-location-arrow"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">Envío incluido</h2>
              <p>🌟 Envíos gratis a tu domicilio en la compra mínima de $100 pesos en Zihuatanejo y Acapulco 🌟</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="100">
            <div class="icon mr-4 align-self-start">
              <span class="icon-reply2"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">Devoluciones gratis</h2>
              <p>✨Si tu compra no ha sido la esperada, no te preocupes, te devolvemos el dinero presentando tu comprobante de compra✨</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="200">
            <div class="icon mr-4 align-self-start">
              <span class="icon-heart"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">¿Aún con dudas?</h2>
              <p>💫Tu compra es segura, te invitamos a ver nuestra sección de recomendaciones 💫</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section site-blocks-2">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0" data-aos="fade" data-aos-delay="100">
            <a class="block-2-item">
              <figure class="image">
                <img src="images/coleccionDeportiva.jpeg" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Ropa</span>
                <h3>Deportiva</h3>
              </div>
            </a>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0" data-aos="fade" data-aos-delay="100">
            <a class="block-2-item" >
              <figure class="image">
                <img src="images/coleccionCasual.jpeg" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Ropa</span>
                <h3>Casual</h3>
              </div>
            </a>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0" data-aos="fade" data-aos-delay="100">
            <a class="block-2-item">
              <figure class="image">
                <img src="images/coleccionAccesorios.jpeg" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Sección</span>
                <h3>Accesorios</h3>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

   <!-- <div class="site-section block-3 site-blocks-2 bg-light">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>Nuestras prendas mas vendidas</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="nonloop-block-3 owl-carousel">
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="images/Prenda 1.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Camiseta Fashion Gris</a></h3>
                    <p class="mb-0">Camiseta para hombre fresca y suave</p>
                    <p class="text-primary font-weight-bold">$250</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="images/prenda4.png" alt="Image placeholder" class="img-fluid prueba">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Top Superman</a></h3>
                    <p class="mb-0">Top para ella 100% algodón</p>
                    <p class="text-primary font-weight-bold">$300</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="images/Prenda 5.png" alt="Image placeholder" class="img-fluid prueba">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Pants-jeans para Ella</a></h3>
                    <p class="mb-0">Cómodos pants para entrenar 100% algodón</p>
                    <p class="text-primary font-weight-bold">$250</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="images/Prenda 7.png" alt="Image placeholder" class="img-fluid prueba">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Jean negro</a></h3>
                    <p class="mb-0">Suave comodidad y perfecto para entrenar</p>
                    <p class="text-primary font-weight-bold">$50</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="images/Prenda 3.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Playera deportiva Batman</a></h3>
                    <p class="mb-0">100% algodón y perfecto para sudar</p>
                    <p class="text-primary font-weight-bold">$50</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
-->


    </br>
    <div class=" block-4  bg-light">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12  text-center pt-8">
          <h2>Conócenos 💙</h2>
          </br>
          <div class="embed-responsive embed-responsive-16by9">
          <iframe lass="embed-responsive-item" src="https://www.youtube.com/embed/fxO-QCQewbU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
         </div>
          </div>
        </div>
        </div>
        </div>

        </br>
        <div class=" block-4  bg-light">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12  text-center pt-8">
          <h2>Escucha nuestro podcast de esta semana 🎧</h2>
          </br>
          <figure>
    <figcaption>Alexander nos cuenta su estilo de vida inspirado en VARO'S: </figcaption>
    <audio
        controls
        src="media/podcastVaros.mp3">
        <a href="media/podcastVaros.mp3">
        Download audio
        </a>
    </audio>
</figure>

         </div>
          </div>
        </div>
        </div>
        </div>






    <?php include("./layouts/footer.php"); ?> 

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>