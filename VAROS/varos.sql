-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-09-2022 a las 00:31:29
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `varos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(400) NOT NULL,
  `imagen` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `descripcion`, `imagen`) VALUES
(1, 'Mujeres', 'Ropar para dama', 'women.jpg'),
(2, 'Hombres', 'Ropa para hombre', 'men.jpg'),
(3, 'Niños', 'Ropa para niños', 'children.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envios`
--

CREATE TABLE `envios` (
  `id_envio` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `ciudad` varchar(50) NOT NULL,
  `calle` varchar(50) NOT NULL,
  `num_calle` varchar(50) NOT NULL,
  `cp` varchar(50) NOT NULL,
  `id_venta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `envios`
--

INSERT INTO `envios` (`id_envio`, `estado`, `ciudad`, `calle`, `num_calle`, `cp`, `id_venta`) VALUES
(1, 'Guerrero', '2', 'Amayaltepec', '12', '39715', 5),
(2, 'Guerrero', '2', 'Amayaltepec', '12', '39715', 6),
(3, 'Guerrero', '2', 'Amayaltepec', '12', '39715', 7),
(4, 'Guerrero', '2', 'Amayaltepec', '12', '39715', 8),
(5, 'Guerrero', '2', 'Amayaltepec', '12', '39715', 9),
(6, 'Guerrero', '2', 'Amayaltepec', '12', '39715', 10),
(7, '', '1', '', '', '', 11),
(8, '', '1', '', '', '', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` double NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `inventario` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `talla` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `precio`, `imagen`, `inventario`, `id_categoria`, `talla`, `color`) VALUES
(1, 'Tank Top', 'Finding perfect t-shirt', 60, 'cloth_1.jpg', 25, 3, 'XL', 'white'),
(2, 'Corater', 'Finding perfect products', 20, 'shoe_1.jpg', -1, 2, '25.5', 'blue'),
(3, 'Top Superman', 'Top para el gym suave y freca', 120, 'prenda4.png', 6, 1, '27', 'blue'),
(54, 'producto 0', 'Descripcion breve del producto generico', 398, 'Prenda 6.png', 91, 1, 'XL', 'multiple'),
(55, 'producto 1', 'Descripcion breve del producto generico', 448, 'Prenda 6.png', 71, 1, 'XL', 'multiple'),
(56, 'producto 2', 'Descripcion breve del producto generico', 189, 'Prenda 6.png', 66, 1, 'XL', 'multiple'),
(57, 'producto 3', 'Descripcion breve del producto generico', 387, 'Prenda 6.png', 33, 1, 'XL', 'multiple'),
(58, 'producto 4', 'Descripcion breve del producto generico', 203, 'Prenda 6.png', 68, 1, 'XL', 'multiple'),
(59, 'producto 5', 'Descripcion breve del producto generico', 789, 'Prenda 6.png', 79, 1, 'XL', 'multiple'),
(60, 'producto 6', 'Descripcion breve del producto generico', 885, 'Prenda 6.png', 91, 1, 'XL', 'multiple'),
(61, 'producto 7', 'Descripcion breve del producto generico', 889, 'Prenda 6.png', 59, 1, 'XL', 'multiple'),
(62, 'producto 8', 'Descripcion breve del producto generico', 705, 'Prenda 6.png', 70, 1, 'XL', 'multiple'),
(63, 'producto 9', 'Descripcion breve del producto generico', 588, 'Prenda 6.png', 100, 1, 'XL', 'multiple'),
(64, 'producto 10', 'Descripcion breve del producto generico', 884, 'Prenda 6.png', 45, 1, 'XL', 'multiple'),
(65, 'producto 11', 'Descripcion breve del producto generico', 934, 'Prenda 6.png', 26, 1, 'XL', 'multiple'),
(66, 'producto 12', 'Descripcion breve del producto generico', 663, 'Prenda 6.png', 7, 1, 'XL', 'multiple'),
(67, 'producto 13', 'Descripcion breve del producto generico', 833, 'Prenda 6.png', 63, 1, 'XL', 'multiple'),
(68, 'producto 14', 'Descripcion breve del producto generico', 917, 'Prenda 6.png', 55, 1, 'XL', 'multiple'),
(69, 'producto 15', 'Descripcion breve del producto generico', 802, 'Prenda 6.png', 70, 1, 'XL', 'multiple'),
(70, 'producto 16', 'Descripcion breve del producto generico', 99, 'Prenda 6.png', 21, 1, 'XL', 'multiple'),
(71, 'producto 17', 'Descripcion breve del producto generico', 51, 'Prenda 6.png', 18, 1, 'XL', 'multiple'),
(72, 'producto 18', 'Descripcion breve del producto generico', 699, 'Prenda 6.png', 18, 1, 'XL', 'multiple'),
(73, 'producto 19', 'Descripcion breve del producto generico', 953, 'Prenda 6.png', 98, 1, 'XL', 'multiple'),
(74, 'producto 20', 'Descripcion breve del producto generico', 885, 'Prenda 6.png', 27, 1, 'XL', 'multiple'),
(75, 'producto 21', 'Descripcion breve del producto generico', 15, 'Prenda 6.png', 52, 1, 'XL', 'multiple'),
(76, 'producto 22', 'Descripcion breve del producto generico', 437, 'Prenda 6.png', 25, 1, 'XL', 'multiple'),
(77, 'producto 23', 'Descripcion breve del producto generico', 331, 'Prenda 6.png', 34, 1, 'XL', 'multiple'),
(78, 'producto 24', 'Descripcion breve del producto generico', 92, 'Prenda 6.png', 27, 1, 'XL', 'multiple'),
(79, 'producto 25', 'Descripcion breve del producto generico', 421, 'Prenda 6.png', 40, 1, 'XL', 'multiple'),
(80, 'producto 26', 'Descripcion breve del producto generico', 130, 'Prenda 6.png', 74, 1, 'XL', 'multiple'),
(81, 'producto 27', 'Descripcion breve del producto generico', 582, 'Prenda 6.png', 1, 1, 'XL', 'multiple'),
(82, 'producto 28', 'Descripcion breve del producto generico', 441, 'Prenda 6.png', 12, 1, 'XL', 'multiple'),
(83, 'producto 29', 'Descripcion breve del producto generico', 70, 'Prenda 6.png', 12, 1, 'XL', 'multiple'),
(84, 'producto 30', 'Descripcion breve del producto generico', 683, 'Prenda 6.png', 96, 1, 'XL', 'multiple'),
(85, 'producto 31', 'Descripcion breve del producto generico', 329, 'Prenda 6.png', 54, 1, 'XL', 'multiple'),
(86, 'producto 32', 'Descripcion breve del producto generico', 869, 'Prenda 6.png', 8, 1, 'XL', 'multiple'),
(87, 'producto 33', 'Descripcion breve del producto generico', 278, 'Prenda 6.png', 16, 1, 'XL', 'multiple'),
(88, 'producto 34', 'Descripcion breve del producto generico', 167, 'Prenda 6.png', 15, 1, 'XL', 'multiple'),
(89, 'producto 35', 'Descripcion breve del producto generico', 593, 'Prenda 6.png', 63, 1, 'XL', 'multiple'),
(90, 'producto 36', 'Descripcion breve del producto generico', 528, 'Prenda 6.png', 11, 1, 'XL', 'multiple'),
(91, 'producto 37', 'Descripcion breve del producto generico', 120, 'Prenda 6.png', 29, 1, 'XL', 'multiple'),
(92, 'producto 38', 'Descripcion breve del producto generico', 664, 'Prenda 6.png', 21, 1, 'XL', 'multiple'),
(93, 'producto 39', 'Descripcion breve del producto generico', 249, 'Prenda 6.png', 26, 1, 'XL', 'multiple'),
(94, 'producto 40', 'Descripcion breve del producto generico', 808, 'Prenda 6.png', 99, 1, 'XL', 'multiple'),
(95, 'producto 41', 'Descripcion breve del producto generico', 776, 'Prenda 6.png', 97, 1, 'XL', 'multiple'),
(96, 'producto 42', 'Descripcion breve del producto generico', 412, 'Prenda 6.png', 43, 1, 'XL', 'multiple'),
(97, 'producto 43', 'Descripcion breve del producto generico', 571, 'Prenda 6.png', 37, 1, 'XL', 'multiple'),
(98, 'producto 44', 'Descripcion breve del producto generico', 636, 'Prenda 6.png', 2, 1, 'XL', 'multiple'),
(99, 'producto 45', 'Descripcion breve del producto generico', 111, 'Prenda 6.png', 98, 1, 'XL', 'multiple'),
(100, 'producto 46', 'Descripcion breve del producto generico', 121, 'Prenda 6.png', 49, 1, 'XL', 'multiple'),
(101, 'producto 47', 'Descripcion breve del producto generico', 524, 'Prenda 6.png', 80, 1, 'XL', 'multiple'),
(102, 'producto 48', 'Descripcion breve del producto generico', 201, 'Prenda 6.png', 18, 1, 'XL', 'multiple'),
(103, 'producto 49', 'Descripcion breve del producto generico', 441, 'Prenda 6.png', 18, 1, 'XL', 'multiple');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_venta`
--

CREATE TABLE `productos_venta` (
  `id` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `precio` double NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos_venta`
--

INSERT INTO `productos_venta` (`id`, `id_venta`, `id_producto`, `cantidad`, `precio`, `subtotal`) VALUES
(1, 1, 3, 7, 120, 840),
(2, 1, 2, 1, 20, 20),
(3, 2, 3, 1, 120, 120),
(4, 4, 3, 1, 120, 120),
(5, 5, 3, 1, 120, 120),
(6, 6, 2, 1, 20, 20),
(7, 7, 3, 1, 120, 120),
(8, 8, 3, 2, 120, 240),
(9, 9, 1, 5, 60, 300),
(10, 10, 3, 4, 120, 480),
(11, 11, 2, 1, 20, 20),
(12, 12, 2, 3, 20, 60);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `telefono` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `img_perfil` varchar(300) NOT NULL DEFAULT 'default.jpg',
  `nivel` varchar(50) NOT NULL DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `telefono`, `email`, `password`, `img_perfil`, `nivel`) VALUES
(1, 'José David Vázquez Rojas', '7445863481', 'david_vrj@outlook.com', '608c647bc5ecc80efda4cf2eb9dc0dbcde54e882', 'default.jpg', 'admin'),
(2, 'José David Vázquez Rojas', '7445863481', 'david_vrj@outlook.com', '9523b38da0f1d5f82c5e961423e9842e54f3abc0', 'default.jpg', 'admin'),
(3, 'José David Vázquez Rojas', '7445863481', 'david_vrj@outlook.com', '6ecb4dd8c55523f05b53ff4eb05668fb5aff6af0', 'default.jpg', 'admin'),
(4, 'José David Vázquez Rojas', '7445863481', 'david_vrj@outlook.com', 'aa743a0aaec8f7d7a1f01442503957f4d7a2d634', 'default.jpg', 'admin'),
(5, 'José David Vázquez Rojas', '7445863481', 'david_vrj@outlook.com', '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'default.jpg', 'admin'),
(6, 'José David Vázquez Rojas', '7445863481', 'david_vrj@outlook.com', '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'default.jpg', 'admin'),
(7, 'José David Vázquez Rojas', '7445863481', 'david_vrj@outlook.com', 'b155b923b2d84e0930954aea6c335b336a49ce8d', 'default.jpg', 'admin'),
(8, ' ', '', '', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'default.jpg', 'admin'),
(9, ' ', '', '', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'default.jpg', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `total` double NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `id_usuario`, `total`, `fecha`) VALUES
(1, 1, 860, '2022-09-22 10:09:25'),
(2, 1, 120, '2022-09-22 12:09:45'),
(3, 1, 0, '2022-09-22 12:09:54'),
(4, 1, 120, '2022-09-22 12:09:07'),
(5, 1, 120, '2022-09-22 12:09:53'),
(6, 1, 20, '2022-09-22 12:09:30'),
(7, 1, 120, '2022-09-22 12:09:55'),
(8, 1, 240, '2022-09-22 12:09:24'),
(9, 1, 300, '2022-09-22 12:09:19'),
(10, 1, 480, '2022-09-22 13:09:17'),
(11, 1, 20, '2022-09-23 13:09:25'),
(12, 1, 60, '2022-09-23 13:09:23');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `envios`
--
ALTER TABLE `envios`
  ADD PRIMARY KEY (`id_envio`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos_venta`
--
ALTER TABLE `productos_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `envios`
--
ALTER TABLE `envios`
  MODIFY `id_envio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT de la tabla `productos_venta`
--
ALTER TABLE `productos_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
